package cz.ladinho10.samplesyncadapter.shared.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
@SuppressWarnings("serial")
public class Contact implements Serializable {

	// Android Id
	private Long contactId;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private boolean changed;
	private boolean deleted;

	public Contact() {
	}

	public Contact(Long contactId, String firstName, String lastName, String phone, String email, Boolean changed, Boolean deleted) {
		this.contactId = contactId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.changed = changed;
		this.deleted = deleted;
	}
	
	public Long getContactId() {
		return contactId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public boolean isChanged() {
		return changed;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
}
