package cz.ladinho10.samplesyncadapter.shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
@SuppressWarnings("serial")
public class User implements Serializable {

	@Id
	private Long userId;
	private String username;
	private String password;
	private List<Contact> contacts = new ArrayList<Contact>();

	private User() {
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public Long getUserId() {
		return userId;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void addContact(Contact contact) {
		contacts.add(contact);
	}
}
