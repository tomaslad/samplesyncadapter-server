package cz.ladinho10.samplesyncadapter.server;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;

import cz.ladinho10.samplesyncadapter.client.login.LoginService;
import cz.ladinho10.samplesyncadapter.shared.model.Contact;
import cz.ladinho10.samplesyncadapter.shared.model.User;

@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements LoginService {

	private Logger logger = Logger.getLogger(getClass().getName());

	public User login(String username, String password, Boolean newUser) {
		ObjectifyService.register(User.class);

		logger.info("username: " + username + "; password: " + password);

		if (newUser) {
			Contact contact1 = new Contact(585001L, "Tomas", "Lad", "111222333", "tomas.lad@email.com", false, false);
			Contact contact2 = new Contact(585002L, "Petr", "Vokurka", "654654654", "petr.vokurka@email.com", false, false);
			User user = new User(username, password);
			user.addContact(contact1);
			user.addContact(contact2);
			
			ObjectifyService.ofy().save().entities(user);
			return user;
		} else {
			List<User> users = ObjectifyService.ofy().load().type(User.class).list();

			logger.info("users.size: " + users.size());

			for (User user : users) {
				if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
					return user;
				}
			}

			return null;
		}
	}
}
