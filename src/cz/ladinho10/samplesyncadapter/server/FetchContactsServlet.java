package cz.ladinho10.samplesyncadapter.server;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.ObjectifyService;

import cz.ladinho10.samplesyncadapter.shared.model.User;

@SuppressWarnings("serial")
public class FetchContactsServlet extends HttpServlet {

	private Logger logger = Logger.getLogger(getClass().getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "Get");
		doIt(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "Post");
		doIt(request, response);
	}

	private void doIt(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ObjectifyService.register(User.class);
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		List<User> users = ObjectifyService.ofy().load().type(User.class).list();
		
		for (User user : users) {
			if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
				
				Gson gson = new Gson();
				String json = gson.toJson(user.getContacts());
				logger.log(Level.INFO, json); 
				
				response.setContentType("application/json; charset=utf-8");
				response.getWriter().write(json);
				
				return;
			}
		}
	}
}
