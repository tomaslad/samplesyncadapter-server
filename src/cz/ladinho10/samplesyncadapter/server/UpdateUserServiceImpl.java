package cz.ladinho10.samplesyncadapter.server;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;

import cz.ladinho10.samplesyncadapter.client.contact.UpdateUserService;
import cz.ladinho10.samplesyncadapter.shared.model.User;

@SuppressWarnings("serial")
public class UpdateUserServiceImpl extends RemoteServiceServlet implements UpdateUserService {

	private Logger logger = Logger.getLogger(getClass().getName());
	
	@Override
	public Boolean update(User user) {
		ObjectifyService.register(User.class);
		try {
			ObjectifyService.ofy().save().entities(user);
		} catch (Exception e) {
			logger.log(Level.WARNING, "update se nezdaril");
			return false;
		}

		logger.log(Level.INFO, "update v pohode");
		return true;
	}
}
