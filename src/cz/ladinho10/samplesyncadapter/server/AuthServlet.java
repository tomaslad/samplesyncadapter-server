package cz.ladinho10.samplesyncadapter.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class AuthServlet extends HttpServlet {

	private static final long serialVersionUID = 8063997809573277179L;
	private Logger logger = Logger.getLogger(getClass().getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "Get");
		doIt(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "Post");
		doIt(request, response);
	}

	private void doIt(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "ContentType: " + request.getContentType());

		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;
		try {
			InputStream inputStream = request.getInputStream();
			if (inputStream != null) {
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				char[] charBuffer = new char[128];
				int bytesRead = -1;
				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ex) {
					throw ex;
				}
			}
		}

		logger.log(Level.INFO, "Body: " + stringBuilder.toString());

		response.setContentType("text/plain");
		response.getWriter().println("Hello, world");
	}
}
