package cz.ladinho10.samplesyncadapter.client.login;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import cz.ladinho10.samplesyncadapter.shared.model.User;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("login")
public interface LoginService extends RemoteService {
	
	User login(String username, String password, Boolean newUser);
}
