package cz.ladinho10.samplesyncadapter.client.login;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;

import cz.ladinho10.samplesyncadapter.client.contact.ContactPanel;
import cz.ladinho10.samplesyncadapter.shared.model.User;

public class Login extends Composite {

	/**
	 * Create a remote service proxy to talk to the server-side Login service.
	 */
	private final LoginServiceAsync loginService = GWT.create(LoginService.class);

	private TextBox textBoxUsername;
	private TextBox textBoxPassword;
	private CheckBox chckbxRememberMeOn;

	public Login() {
		VerticalPanel verticalPanel = new VerticalPanel();
		initWidget(verticalPanel);

		Label lblLoginToYour = new Label("Sign in to your account");
		lblLoginToYour.setStyleName("gwt-Label-Login");
		verticalPanel.add(lblLoginToYour);

		FlexTable flexTable = new FlexTable();
		verticalPanel.add(flexTable);
		flexTable.setWidth("345px");

		Label lblUsername = new Label("Username:");
		lblUsername.setStyleName("gwt-Label-Login");
		flexTable.setWidget(0, 0, lblUsername);

		textBoxUsername = new TextBox();
		flexTable.setWidget(0, 1, textBoxUsername);

		Label lblPassword = new Label("Password:");
		lblPassword.setStyleName("gwt-Label-Login");
		flexTable.setWidget(1, 0, lblPassword);

		textBoxPassword = new TextBox();
		textBoxPassword.setDirection(Direction.RTL);
		flexTable.setWidget(1, 1, textBoxPassword);

		chckbxRememberMeOn = new CheckBox("Create a new user?");
		chckbxRememberMeOn.setStyleName("gwt-Login-CheckBox");
		flexTable.setWidget(2, 1, chckbxRememberMeOn);

		Button btnSignIn = new Button("Sign In");
		btnSignIn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String username = textBoxUsername.getText();
				String password = textBoxPassword.getText();
				Boolean newUser = chckbxRememberMeOn.getValue();
				if (username.length() == 0 || password.length() == 0) {
					Window.alert("Username or password is empty.");
				}

				loginService.login(username, password, newUser, new AsyncCallback<User>() {

					@Override
					public void onSuccess(User result) {
						Window.alert("Login.onSuccess... " + result);
						RootPanel.get().clear();
						//RootPanel.get().add(new Label("Lol lol lolik"));
						RootPanel.get().add(new ContactPanel(result));
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Login.onFailure... " + caught.getMessage());
					}
				});
			}
		});
		flexTable.setWidget(3, 1, btnSignIn);
	}
}
