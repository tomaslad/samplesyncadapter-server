package cz.ladinho10.samplesyncadapter.client.login;

import com.google.gwt.user.client.rpc.AsyncCallback;

import cz.ladinho10.samplesyncadapter.shared.model.User;

public interface LoginServiceAsync {

	void login(String username, String password, Boolean newUser, AsyncCallback<User> callback);
}
