package cz.ladinho10.samplesyncadapter.client.contact;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import cz.ladinho10.samplesyncadapter.shared.model.User;

@RemoteServiceRelativePath("updateUser")
public interface UpdateUserService extends RemoteService {

	Boolean update(User user);
}
