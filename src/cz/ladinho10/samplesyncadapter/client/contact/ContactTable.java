package cz.ladinho10.samplesyncadapter.client.contact;

import java.util.List;

import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ListDataProvider;

import cz.ladinho10.samplesyncadapter.shared.model.Contact;
import cz.ladinho10.samplesyncadapter.shared.model.User;

public class ContactTable extends CellTable<Contact> {

	public ContactTable(User user) {
		// Create first name column.
		TextColumn<Contact> firstNameColumn = new TextColumn<Contact>() {
			@Override
			public String getValue(Contact contact) {
				return contact.getFirstName();
			}
		};

		// Create email column.
		TextColumn<Contact> emailColumn = new TextColumn<Contact>() {
			@Override
			public String getValue(Contact contact) {
				return contact.getEmail();
			}
		};

		// Add the columns.
		addColumn(firstNameColumn, "First Name");
		addColumn(emailColumn, "Email");

		// Create a data provider.
		ListDataProvider<Contact> dataProvider = new ListDataProvider<Contact>();

		// Connect the table to the data provider.
		dataProvider.addDataDisplay(this);

		// Add the data to the data provider, which automatically pushes it to
		// the
		// widget.
		List<Contact> list = dataProvider.getList();
		for (Contact contact : user.getContacts()) {
			list.add(contact);
		}
	}

}
