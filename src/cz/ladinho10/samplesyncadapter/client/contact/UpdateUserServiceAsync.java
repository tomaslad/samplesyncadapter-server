package cz.ladinho10.samplesyncadapter.client.contact;

import com.google.gwt.user.client.rpc.AsyncCallback;

import cz.ladinho10.samplesyncadapter.shared.model.User;

public interface UpdateUserServiceAsync {

	void update(User user, AsyncCallback<Boolean> callback);
}
