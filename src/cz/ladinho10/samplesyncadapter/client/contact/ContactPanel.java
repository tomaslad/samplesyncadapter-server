package cz.ladinho10.samplesyncadapter.client.contact;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;

import cz.ladinho10.samplesyncadapter.shared.model.User;

public class ContactPanel extends VerticalPanel {

	public ContactPanel(User user) {
		ContactTable table = new ContactTable(user);
		add(table);
		
		Button button = new Button("Save");
		add(button);
		
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
			}
		});
	}
}
